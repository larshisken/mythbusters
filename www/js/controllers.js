angular.module('mythBusters.controllers', ['tc.chartjs'])

.controller('AppCtrl', function ($scope) {
	
})

.controller('EpisodesIndexCtrl', function ($scope, Episodes) {
	$scope.episodes = Episodes.all();
})

.controller('EpisodesDetailCtrl', function ($scope, $stateParams, Episodes) {
	$scope.episode = Episodes.get($stateParams.episodeId);
})

.controller('MythsDetailCtrl', function ($scope, $stateParams, Myths) {
	$scope.myth = Myths.get($stateParams.mythId);

	/*
	 * Toggle buttons
	 */
  $scope.setActive = function(type) {
    $scope.active = type;
  };
  $scope.isActive = function(type) {
    return type === $scope.active;
  };

  /*
   * Add data to tc.chartjs
   */
  $scope.chartData = {
  	labels: ['< 10min', '< 20min', '< 30min'],
  	datasets: [
  		{
  			label: 'Confirmed',
  			strokeColor: 'green',
  			pointColor: 'green',
  			data: [43, 47, 40]
  		},
  		{
  			label: 'Plausible',
  			strokeColor: 'orange',
  			pointColor: 'orange',
  			data: [20, 18, 32]
  		},
  		{
  			label: 'Busted',
  			strokeColor: 'red',
  			pointColor: 'red',
  			data: [37, 35, 28]
  		}
  	]
  }
  $scope.chartOpts = {
  	responsive: true,
  	scaleShowGridLines: false,
  	datasetFill: false,
  	bezierCurve: false
  }

  /*
   * Comments section
   */
  $scope.comments = [
  	{
  		name: "Joost",
  		text: "100% CONFIRMED, vaak genoeg gedaan."
  	},
  	{
  		name: "Arjen",
  		text: "Dit is echt onzin! BUSTED."
  	},
  	{
  		name: "Jip",
  		text: "Waanzin!"
  	}
  ];
  $scope.placeComment = function() {
  	if ($scope.yourComment != undefined) {
  		var comment = {
  			name: "Lars",
  			text: $scope.yourComment
  		}
  		$scope.comments.unshift(comment);
    	$scope.yourComment = undefined;
  	}
  }
})

.controller('StatsCtrl', function ($scope) {
	$scope.chartData = [
		{
			value: 56,
			color: 'green',
			label: 'Goed',
		},
		{
			value: 44,
			color: 'red',
			label: 'Fout'
		}
	];
	$scope.chartOpts = {
		responsive: true,
		segmentShowStroke: false
	}
});