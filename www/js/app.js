angular.module('mythBusters', ['ionic', 'mythBusters.controllers', 'mythBusters.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($ionicConfigProvider, $stateProvider, $urlRouterProvider) {
	$ionicConfigProvider.backButton.text('').icon('ion-chevron-left').previousTitleText(false);
	$ionicConfigProvider.navBar.alignTitle('center');

	$stateProvider
		.state('app', {
			url: '',
			abstract: true,
			templateUrl: 'templates/menu.html',
			controller: 'AppCtrl'
		})
		.state('app.episodes', {
			url: '/episodes',
			views: {
				'menuContent': {
					templateUrl: 'templates/episodes-index.html',
					controller: 'EpisodesIndexCtrl'
				}
			}
		})
		.state('app.episodes-detail', {
			url: '/episodes/:episodeId',
			views: {
				'menuContent': {
					templateUrl: 'templates/episodes-detail.html',
					controller: 'EpisodesDetailCtrl'
				}
			}
		})
		.state('app.myths', {
			url: '/myths'
		})
		.state('app.myths-detail', {
			url: '/myths/:mythId',
			views: {
				'menuContent': {
					templateUrl: 'templates/myths-detail.html',
					controller: 'MythsDetailCtrl'
				}
			}
		})
		.state('app.stats', {
			url: '/stats',
			views: {
				'menuContent': {
					templateUrl: 'templates/stats.html',
					controller: 'StatsCtrl'
				}
			}
		})

		$urlRouterProvider.otherwise('/episodes');
});