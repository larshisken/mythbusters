var episodes = [
    {
      "_id": "5506dbde7a1c3eb74fc970af",
      "title": "Video Games Special",
      "air_date": "1428429600000",
      "thumbnail": "mythbusters-225-03.jpg",
      "image": "mythbusters-225-03-full.jpg",
      "description": "For the debut of MythBusters Season 10, Adam and Jamie tackle the greatest animated TV series of all time: The Simpsons.",
      "myths": [
        {
          "_id": "5506dbde3f5afed13af51ed2",
          "title": "Can you easily carry and deploy a large variety of weapons, as in Doom?",
          "description": "In an abandoned building at Mare Island Naval Shipyard, Adam and Jamie set up an environment to replicate a Doom game level with advice from id Software creative director Tim Willits.",
          "status": "Plausible",
          "episodeId": "5506dbde7a1c3eb74fc970af"
        },
        {
          "_id": "5506e6682c428f8247c95b2a",
          "title": "Can you easily slice through large amounts of thrown fruit, as in Fruit Ninja?",
          "description": "Adam played three 60-second time trials of the game, using a wooden practice sword as the motion controller, and averaged 99.3 hits. At Hunters Point Naval Shipyard, he and Jamie set up an elevated platform for Adam to stand on and slice fruit with an actual katana as it was thrown up to him by three professional jugglers.",
          "status": "Busted",
          "episodeId": "5506dbde7a1c3eb74fc970af"
        }
      ]
    },
		{
			"_id": "550582f1e0cf5cc3114dc5c2",
			"title": "The Simpsons Special",
			"air_date": "1428429600000",
			"thumbnail": "mythbusters-227-06.jpg",
      "image": "mythbusters-227-06-full.jpg",
			"description": "For the debut of MythBusters Season 10, Adam and Jamie tackle the greatest animated TV series of all time: The Simpsons.",
			"myths": [
  			{
  				"_id": "5505865a07bad13308037a2d",
  				"title": "Will a cherry bomb dropped in a school toilet make others act like geysers?",
  				"description": "If a lit cherry bomb is flushed down a toilet, it will send water shooting out of every toilet attached to the same plumbing system when it explodes. Based on the episode The Crepes of Wrath.",
  				"status": "Busted",
  				"episodeId": "550582f1e0cf5cc3114dc5c2"
  			},
  			{
  				"_id": "5505865a59d30501f2c3d6e6",
  				"title": "Will placing someone between a wrecking ball and a building protect the building?",
  				"description": "A person hanging onto a wrecking ball can protect a house from damage if the ball swings to pin him against the wall. Based on the episode Sideshow Bob Roberts.",
  				"status": "Plausible",
  				"episodeId": "550582f1e0cf5cc3114dc5c2"
  			}
			]
		},
		{
			"_id": "550582f169107b4bb4048450",
			"title": "The A-Team Special",
			"air_date": "1428688800000",
			"thumbnail": "mythbusters-226-05.jpg",
      "image": "mythbusters-226-05-full.jpg",
			"description": "On the MythBusters A-Team special, Jamie and Adam find out if it's really possible to build a working cannon out of stuff abandoned in a barn, then test whether a sewer explosion can really take out the bad guys without serious injury.",
			"myths": [
  			{
  				"_id": "5505865a508a15a479585eab",
  				"title": "Can you build a propane-powered log cannon to shoot planks at enemies?",
  				"description": "A cannon fashioned from a log can fire half-width 2-by-4 planks, fueled by a propane explosion and mounted on a forklift.",
  				"status": "Busted",
  				"episodeId": "550582f169107b4bb4048450"
  			},
  			{
  				"_id": "5505865acd4bd25bc6e22050",
  				"title": "Can you safely disable a pursuing car by blasting a manhole thumbnail upward into it?",
  				"description": "An explosion inside a sewer can propel a manhole thumbnail up into a pursuing car, disabling it without injuring the people inside or any bystanders.",
  				"status": "Plausible",
  				"episodeId": "550582f169107b4bb4048450"
  			}
			]
		},
    {
      "_id": "5506dbde8b29994819f9ebe7",
      "title": "The Busters of the Lost Myths",
      "air_date": "1426348800000",
      "thumbnail": "mythbusters-221-07.jpg",
      "image": "mythbusters-221-07-full.jpg",
      "description": "It is possible to survive a chamber full of wall-mounted poison dart launchers by running past them. Based on Indiana Jones' theft of the golden idol in the opening scene.",
      "myths": [
        {
          "_id": "5506dbde41a40631cbd9eefe",
          "title": "Could Indiana Jones have outrun a battery of wall-mounted dart launchers?",
          "description": "It is possible to survive a chamber full of wall-mounted poison dart launchers by running past them. Based on Indiana Jones' theft of the golden idol in the opening scene.",
          "status": "Plausible",
  				"episodeId": "5506dbde8b29994819f9ebe7"
        },
        {
          "_id": "5506dbded5af4cabe6bbace2",
          "title": "Can you use a whip to disarm or neutralize an enemy with a gun?",
          "description": "It is possible to disarm or neutralize a pistol-wielding opponent by targeting the gun hand with a whip.",
          "status": "Plausible",
  				"episodeId": "5506dbde8b29994819f9ebe7"
        },
        {
          "_id": "5506dbde8166bf0f7fcf5b3b",
          "title": "Does the tip of a whip break the sound barrier?",
          "description": "The tip of a whip will break the sound barrier when it cracks.",
          "status": "Confirmed",
  				"episodeId": "5506dbde8b29994819f9ebe7"
        },
        {
          "_id": "5506dbde1b1bd1fbba764d36",
          "title": "Can you use a whip to swing safely across a chasm?",
          "description": "It is possible to snag an overhead projection with a whip and swing safely across a chasm. Based on the opening scene.",
          "status": "Plausible",
  				"episodeId": "5506dbde8b29994819f9ebe7"
        }
      ]
    }
	];

angular.module('mythBusters.services', [])

.factory('Episodes', function() {
	return {
		all: function() {
			return episodes;
		},
		get: function(episodeId) {
			return episodes.filter(function(e){return e._id == episodeId})[0];
		}
	}
})

.factory('Myths', function() {
	return {
		all: function() {
			return [].concat.apply([], episodes.map(function(e){return e.myths}));
		},
		get: function(mythId) {
			var myths = [].concat.apply([], episodes.map(function(e){return e.myths}));
			return myths.filter(function(e){return e._id == mythId})[0];
		}
	}
});